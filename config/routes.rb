Rails.application.routes.draw do
  get 'users/index'

  resources :users
  get 'login/index'
  post 'login/sign_in'
  get 'login/sign_out'
  # get 'users/new'
  # post 'users/create'
  root 'users#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
