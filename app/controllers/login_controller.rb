class LoginController < ApplicationController
    def index
    end

    def sign_in
        @user = User.find_by(email: params[:session][:email].downcase)
        if(@user && @user.password == params[:session][:password])
            log_in @user
            redirect_to users_path
        else
            flash[:danger] = 'Invalid email/password combination'
            redirect_to login_index_path
        end
    end
    
    def sign_out
        log_out
        redirect_to login_index_path
    end
end
