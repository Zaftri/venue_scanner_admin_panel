class User < ApplicationRecord
    
    @EMAIL_REGEX = /\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\z/i
    validates :firstname, presence: true
    validates :surname, presence: true
    validates :email, presence: true, :format => @EMAIL_REGEX
    validates :password, presence: true
    validates_confirmation_of :password, message: "must be the same"
    validates_length_of :password, in: 6..20, message: "must be strong (from 6 to 20 digits)"
    validates_uniqueness_of :email, message: "must be unique"

end
